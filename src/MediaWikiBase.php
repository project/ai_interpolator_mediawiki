<?php

namespace Drupal\ai_interpolator_mediawiki;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Media Wiki Context Fetcher.
 */
class MediaWikiBase extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Wikipedia / MediaWiki';

  /**
   * The MediaWiki api.
   */
  public WikiApi $api;

  /**
   * The token system to replace and generate paths.
   */
  public Token $token;

  /**
   * Construct an Text Long field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\ai_interpolator_dreamstudio\WikiApi $api
   *   The MediaWiki api.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    WikiApi $api,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->api = $api;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ai_interpolator_mediawiki.api'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function allowedInputs() {
    return [
      'text',
      'string',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form['interpolator_mediawiki_host'] = [
      '#type' => 'textfield',
      '#title' => 'Custom Host',
      '#description' => $this->t('If you want to use some custom MediaWiki instead of English Wikipedia, please give the api endpoint or change the language prefix here.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_mediawiki_host', "https://en.wikipedia.org/w/api.php"),
      '#weight' => 24,
    ];

    $form['interpolator_mediawiki_search'] = [
      '#type' => 'checkbox',
      '#title' => 'Search before fetching',
      '#description' => $this->t('If you check this it will do a search on your text and take the first result. This is good for misspellings and uncertain search words.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_mediawiki_search', 1),
      '#weight' => 27,
    ];

    $form['interpolator_mediawiki_extract'] = [
      '#type' => 'select',
      '#title' => 'What portion to get',
      '#options' => [
        'full' => $this->t('Full page'),
        'extract' => $this->t('Extract'),
      ],
      '#description' => $this->t('If you want to get the whole page or just the extract.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_mediawiki_extract', "full"),
      '#weight' => 26,
    ];

    $form['interpolator_mediawiki_strip_html'] = [
      '#type' => 'checkbox',
      '#title' => 'Strip HTML',
      '#description' => $this->t('If you enable this, it will strip all the HTML and just keep the text.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_mediawiki_strip_html', 1),
      '#weight' => 28,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    foreach ($entity->{$interpolatorConfig['base_field']} as $wrapperEntity) {
      if (!empty($wrapperEntity->value)) {
        $searchWord = $wrapperEntity->value;
        if ($interpolatorConfig['mediawiki_search']) {
          $results = $this->api->search($wrapperEntity->value, $interpolatorConfig['mediawiki_host']);
          if (isset($results[1][0])) {
            $searchWord = $results[1][0];
          }
        }
        $result = $this->api->getPage($searchWord, $interpolatorConfig['mediawiki_host'], $interpolatorConfig['mediawiki_extract']);

        if (!empty($result['query']['pages'])) {
          $value = $result['query']['pages'][key($result['query']['pages'])]['extract'];
          if ($interpolatorConfig['mediawiki_strip_html']) {
            $value = str_replace('</p>', "\n\n", $value);
            $value = str_replace('<br />', "\n", $value);
            $value = trim(strip_tags($value));
          }
          $values[] = $value;
        }
      }
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Has to have a link an be valid.
    if (empty($value) || !is_string($value)) {
      return FALSE;
    }

    // Otherwise it is ok.
    return TRUE;
  }

}
