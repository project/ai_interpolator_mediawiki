<?php

namespace Drupal\ai_interpolator_mediawiki;

use GuzzleHttp\Client;

/**
 * MediaWiki API helper.
 */
class WikiApi {

  /**
   * The http client.
   */
  protected Client $client;

  /**
   * Constructs a new MediaWiki object.
   *
   * @param \GuzzleHttp\Client $client
   *   Http client.
   */
  public function __construct(Client $client) {
    $this->client = $client;
  }

  /**
   * Search for a page.
   *
   * @param string $word
   *   The word to search.
   * @param string $endpoint
   *   The endpoint to use.
   *
   * @return array|null
   *   The result.
   */
  public function search($word, $endpoint) {
    $query = [
      'action' => 'opensearch',
      'format' => 'json',
      'search' => $word,
      'limit' => 1,
    ];

    return json_decode($this->makeRequest($endpoint, $query)->getContents(), TRUE);
  }

  /**
   * Create image from text.
   *
   * @param string $page
   *   The page to use.
   * @param string $endpoint
   *   The endpoint to use.
   * @param string $portion
   *   Full or extract.
   *
   * @return array|null
   *   The result.
   */
  public function getPage($page, $endpoint, $portion = 'full') {
    $query = [
      'action' => 'query',
      'format' => 'json',
      'prop' => 'extracts',
      'titles' => $page,
      'redirects' => 'true',
    ];
    if ($portion == 'extract') {
      $query['exintro'] = TRUE;
    }

    return json_decode($this->makeRequest($endpoint, $query)->getContents(), TRUE);
  }

  /**
   * Make MediaWiki call.
   *
   * @param string $endpoint
   *   The path.
   * @param array $query_string
   *   The query string.
   * @param string $method
   *   The method.
   * @param string $body
   *   Data to attach if POST/PUT/PATCH.
   * @param array $options
   *   Extra headers.
   *
   * @return string|object
   *   The return response.
   */
  protected function makeRequest($endpoint, array $query_string = [], $method = 'GET', $body = '', array $options = []) {
    // We can wait some.
    $options['connect_timeout'] = 30;
    $options['read_timeout'] = 30;

    if ($body) {
      $options['body'] = $body;
    }

    $new_url = $endpoint;
    $new_url .= count($query_string) ? '?' . http_build_query($query_string) : '';

    $res = $this->client->request($method, $new_url, $options);

    return $res->getBody();
  }

}
