<?php

namespace Drupal\ai_interpolator_mediawiki\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_mediawiki\MediaWikiBase;

/**
 * The rules for a string_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_mediawiki_string_long",
 *   title = @Translation("Wikipedia / MediaWiki"),
 *   field_rule = "string_long"
 * )
 */
class MediaWikiToString extends MediaWikiBase implements AiInterpolatorFieldRuleInterface {

}
