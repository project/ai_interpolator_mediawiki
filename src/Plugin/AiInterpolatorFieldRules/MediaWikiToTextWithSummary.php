<?php

namespace Drupal\ai_interpolator_mediawiki\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_mediawiki\MediaWikiBase;

/**
 * The rules for a text_with_summary field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_mediawiki_text_with_summary",
 *   title = @Translation("Wikipedia / MediaWiki"),
 *   field_rule = "text_with_summary"
 * )
 */
class MediaWikiToTextWithSummary extends MediaWikiBase implements AiInterpolatorFieldRuleInterface {

}
