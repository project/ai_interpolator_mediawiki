<?php

namespace Drupal\ai_interpolator_mediawiki\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_mediawiki\MediaWikiBase;

/**
 * The rules for a text_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_mediawiki_text_long",
 *   title = @Translation("Wikipedia / MediaWiki"),
 *   field_rule = "text_long"
 * )
 */
class MediaWikiToText extends MediaWikiBase implements AiInterpolatorFieldRuleInterface {

}
